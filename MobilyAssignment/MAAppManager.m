//
//  MAAppManager.m
//  MobilyAssignment
//
//  Created by Parag Dulam on 27/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "MAAppManager.h"
#import "Wasfa.h"
#import "Sheet.h"
#import "AppDelegate.h"

@implementation MAAppManager



+(Sheet *) getSheetForId:(NSString *) sheet_id
{
    Sheet *sheet = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Sheet"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sheet_id == %@",sheet_id];
    [fetchRequest setPredicate:predicate];
    NSArray *sheets = [[MAAppManager context] executeFetchRequest:fetchRequest error:nil];
    if (sheets.count) {
        sheet = [sheets firstObject];
    } else {
        sheet = [NSEntityDescription insertNewObjectForEntityForName:@"Sheet" inManagedObjectContext:[MAAppManager context]];
    }
    return sheet;
}

+(Wasfa *) getWasfaForId:(NSString *) wasfa_id
{
    Wasfa *wasfa = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Wasfa"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"data_id == %@",wasfa_id];
    [fetchRequest setPredicate:predicate];
    NSArray *wasfas = [[MAAppManager context] executeFetchRequest:fetchRequest error:nil];
    if (wasfas.count) {
        wasfa = [wasfas firstObject];
    } else {
        wasfa = [NSEntityDescription insertNewObjectForEntityForName:@"Wasfa" inManagedObjectContext:[MAAppManager context]];
    }
    return wasfa;
}


+(AppDelegate *) appDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+(NSManagedObjectContext *) context
{
    return [MAAppManager appDelegate].managedObjectContext;
}

+(void) saveWasfas:(NSArray *)wasfas
{
    for (NSDictionary *wasfaDict in wasfas) {
        [MAAppManager saveWasfa:wasfaDict];
    }
}

+(NSArray *) saveSheets:(NSArray *)sheets
{
    NSMutableArray *retVal = [[NSMutableArray alloc] init];
    for (NSDictionary *sheet in sheets) {
        [retVal addObject:[MAAppManager saveSheet:sheet]];
    }
    return retVal;
}

+(Sheet *) saveSheet:(NSDictionary *) sheetDict
{
    Sheet *sheet = nil;
    NSString *sheet_id = [sheetDict objectForKey:@"id"];
    sheet = [MAAppManager getSheetForId:sheet_id];
    
    sheet.sheet_id = sheet_id;
    sheet.object = [sheetDict objectForKey:@"object"];
    sheet.type = [sheetDict objectForKey:@"type"];
    sheet.sheet_description = [sheetDict objectForKey:@"description"];
    sheet.updated_time = [sheetDict objectForKey:@"updated_time"];
    sheet.view_order = [sheetDict objectForKey:@"view_order"];
    sheet.time_word = [sheetDict objectForKey:@"time_word"];
    
    [[MAAppManager context] save:nil];
    return sheet;
}

+(void) saveWasfa:(NSDictionary *) wasfaDict
{
    Wasfa *wasfa = nil;
    NSString *wasfa_id = [wasfaDict objectForKey:@"id"];
    wasfa = [MAAppManager getWasfaForId:wasfa_id];
    wasfa.sheets = [NSSet setWithArray:[MAAppManager saveSheets:[wasfaDict objectForKey:@"sheets"]]];
    wasfa.data_id = wasfa_id;
    wasfa.title = [wasfaDict objectForKey:@"title"];
    wasfa.category_id = [wasfaDict objectForKey:@"category_id"];
    wasfa.category_name = [wasfaDict objectForKey:@"category_name"];
    wasfa.category_pic = [wasfaDict objectForKey:@"category_pic"];
    wasfa.category_description = [wasfaDict objectForKey:@"description"];
    wasfa.user_id = [wasfaDict objectForKey:@"user_id"];
    wasfa.user_name = [wasfaDict objectForKey:@"user_name"];
    wasfa.user_pic = [wasfaDict objectForKey:@"user_pic"];
    wasfa.cover = [wasfaDict objectForKey:@"cover"];
    wasfa.created_time = [wasfaDict objectForKey:@"created_time"];
    wasfa.likes_count = [wasfaDict objectForKey:@"likes_count"];
    wasfa.comments_count = [wasfaDict objectForKey:@"comments_count"];
    wasfa.views_count = [wasfaDict objectForKey:@"views_count"];
    wasfa.visitor_count = [wasfaDict objectForKey:@"visitor_count"];
    wasfa.is_featured = [wasfaDict objectForKey:@"is_featured"];
    wasfa.time_word = [wasfaDict objectForKey:@"time_word"];
    wasfa.url = [wasfaDict objectForKey:@"url"];
    wasfa.is_liked = [wasfaDict objectForKey:@"is_liked"];
    wasfa.is_followed = [wasfaDict objectForKey:@"is_followed"];
    
    [[MAAppManager context] save:nil];
}



@end
