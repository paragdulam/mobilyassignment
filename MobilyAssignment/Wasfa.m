//
//  Wasfa.m
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "Wasfa.h"
#import "Sheet.h"


@implementation Wasfa

@dynamic category_description;
@dynamic category_id;
@dynamic category_name;
@dynamic category_pic;
@dynamic comments_count;
@dynamic cover;
@dynamic created_time;
@dynamic data_id;
@dynamic is_featured;
@dynamic is_followed;
@dynamic is_liked;
@dynamic likes_count;
@dynamic time_word;
@dynamic title;
@dynamic url;
@dynamic user_id;
@dynamic user_name;
@dynamic user_pic;
@dynamic views_count;
@dynamic visitor_count;
@dynamic sheets;

@end
