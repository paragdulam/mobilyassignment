//
//  Sheet.h
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Sheet : NSManagedObject

@property (nonatomic, retain) NSString * object;
@property (nonatomic, retain) NSString * sheet_description;
@property (nonatomic, retain) NSString * sheet_id;
@property (nonatomic, retain) NSString * time_word;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * updated_time;
@property (nonatomic, retain) NSString * view_order;

@end
