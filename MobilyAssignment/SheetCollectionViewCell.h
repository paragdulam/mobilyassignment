//
//  SheetCollectionViewCell.h
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Sheet;

@interface SheetCollectionViewCell : UICollectionViewCell


-(void)setSheet:(Sheet *)sheet;

@end
