//
//  WasfaTableViewCell.m
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "WasfaTableViewCell.h"
#import "Wasfa.h"
#import "MAImageView.h"


@interface WasfaTableViewCell()

@property(nonatomic) UIView *backgroundView;
@property(nonatomic) MAImageView *profilePicView;
@property(nonatomic) MAImageView *objectImageView;
@property(nonatomic) UILabel *nameLabel;
@property(nonatomic) UILabel *descriptionLabel;
@property(nonatomic) UILabel *subtitleLabel;
@property(nonatomic) UILabel *shareStatisticsLabel;
@property(nonatomic) UIButton *commentsButton;
@property(nonatomic) UIButton *likesButton;
@property(nonatomic) UIButton *viewsButton;


@end

@implementation WasfaTableViewCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.profilePicView = [[MAImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.profilePicView];
        
        self.objectImageView = [[MAImageView alloc] initWithFrame:CGRectZero];
        self.objectImageView.clipsToBounds = NO;
        [self.contentView addSubview:self.objectImageView];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.nameLabel];
        
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.subtitleLabel.font = [UIFont systemFontOfSize:12.f];
        self.subtitleLabel.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.subtitleLabel];

        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:self.descriptionLabel];
        
        self.shareStatisticsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.shareStatisticsLabel.font = [UIFont boldSystemFontOfSize:14.f];
        [self.contentView addSubview:self.shareStatisticsLabel];

        self.likesButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.likesButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
        [self.likesButton setBackgroundColor:[UIColor colorWithRed:239.f/255.f green:241.f/255.f blue:242.f/255.f alpha:1]];
        [self.contentView addSubview:self.likesButton];
        
        self.commentsButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.commentsButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
        [self.commentsButton setBackgroundColor:[UIColor colorWithRed:239.f/255.f green:241.f/255.f blue:242.f/255.f alpha:1]];
        [self.contentView addSubview:self.commentsButton];
        
        self.viewsButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.viewsButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
        [self.viewsButton setBackgroundColor:[UIColor colorWithRed:239.f/255.f green:241.f/255.f blue:242.f/255.f alpha:1]];
        [self.contentView addSubview:self.viewsButton];
    }
    return self;
}


-(void) setWasfa:(Wasfa *) wasfa
{
    self.descriptionLabel.text = wasfa.title;
    [self.descriptionLabel sizeToFit];
    
    self.nameLabel.text = wasfa.user_name;
    [self.nameLabel sizeToFit];
    
    self.subtitleLabel.text = wasfa.category_name;
    [self.subtitleLabel sizeToFit];

    [self.profilePicView setImageURL:[NSURL URLWithString:wasfa.user_pic] forFileId:wasfa.user_id];
    [self.objectImageView setImageURL:[NSURL URLWithString:wasfa.cover] forFileId:wasfa.data_id];
    
    self.shareStatisticsLabel.textAlignment = NSTextAlignmentRight;
    self.shareStatisticsLabel.text = [NSString stringWithFormat:@"Views %@ Comments %@ Likes %@ ",wasfa.views_count,wasfa.comments_count,wasfa.likes_count];
    
    [self.viewsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.commentsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.likesButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [self.viewsButton setTitle:@"Share" forState:UIControlStateNormal];
    [self.commentsButton setTitle:@"Comment" forState:UIControlStateNormal];
    [self.likesButton setTitle:@"Like" forState:UIControlStateNormal];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    self.contentView.frame = CGRectMake(10, 10, self.contentView.frame.size.width - 20, self.contentView.frame.size.height - 20);
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.layer.cornerRadius = 2.f;
    self.contentView.clipsToBounds = YES;
    
    self.contentView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.shadowOffset = CGSizeMake(2.f, 2.f);
    
    self.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.borderWidth = 1.f;
    
    
    self.profilePicView.frame = CGRectMake(self.contentView.frame.size.width - 50, 5, 45, 45);
    self.profilePicView.backgroundColor = [UIColor clearColor];
    
    self.nameLabel.frame = CGRectMake(self.contentView.frame.size.width - self.nameLabel.frame.size.width - self.profilePicView.frame.size.width - 10,
                                      self.profilePicView.frame.origin.y,
                                      self.nameLabel.frame.size.width,
                                      self.nameLabel.frame.size.height);
    
    self.subtitleLabel.frame = CGRectMake(self.contentView.frame.size.width - self.subtitleLabel.frame.size.width - self.profilePicView.frame.size.width - 10,
                                      CGRectGetMaxY(self.nameLabel.frame),
                                      self.subtitleLabel.frame.size.width,
                                      self.subtitleLabel.frame.size.height);
    
    self.descriptionLabel.frame = CGRectMake(self.contentView.frame.size.width - self.descriptionLabel.frame.size.width - 5,
                                             CGRectGetMaxY(self.profilePicView.frame) + 5,
                                             self.descriptionLabel.frame.size.width,
                                             self.descriptionLabel.frame.size.height);
    
    self.objectImageView.frame = CGRectMake(-2.f,
                                            CGRectGetMaxY(self.descriptionLabel.frame) + 5,
                                            self.contentView.frame.size.width + 2.f,
                                            220.f);
    
    self.shareStatisticsLabel.frame = CGRectMake(0, CGRectGetMaxY(self.objectImageView.frame), self.contentView.frame.size.width, 25.f);
    self.viewsButton.frame = CGRectMake(0, CGRectGetMaxY(self.shareStatisticsLabel.frame), self.contentView.frame.size.width/3, 25.f);
    self.likesButton.frame = CGRectMake(CGRectGetMaxX(self.viewsButton.frame), CGRectGetMaxY(self.shareStatisticsLabel.frame), self.contentView.frame.size.width/3, 25.f);
    self.commentsButton.frame = CGRectMake(CGRectGetMaxX(self.likesButton.frame), CGRectGetMaxY(self.shareStatisticsLabel.frame), self.contentView.frame.size.width/3, 25.f);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
