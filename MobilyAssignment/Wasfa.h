//
//  Wasfa.h
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sheet;

@interface Wasfa : NSManagedObject

@property (nonatomic, retain) NSString * category_description;
@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * category_name;
@property (nonatomic, retain) NSString * category_pic;
@property (nonatomic, retain) NSString * comments_count;
@property (nonatomic, retain) NSString * cover;
@property (nonatomic, retain) NSString * created_time;
@property (nonatomic, retain) NSString * data_id;
@property (nonatomic, retain) NSString * is_featured;
@property (nonatomic, retain) NSString * is_followed;
@property (nonatomic, retain) NSString * is_liked;
@property (nonatomic, retain) NSString * likes_count;
@property (nonatomic, retain) NSString * time_word;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * user_name;
@property (nonatomic, retain) NSString * user_pic;
@property (nonatomic, retain) NSString * views_count;
@property (nonatomic, retain) NSString * visitor_count;
@property (nonatomic, retain) NSSet *sheets;
@end

@interface Wasfa (CoreDataGeneratedAccessors)

- (void)addSheetsObject:(Sheet *)value;
- (void)removeSheetsObject:(Sheet *)value;
- (void)addSheets:(NSSet *)values;
- (void)removeSheets:(NSSet *)values;

@end
