//
//  WasfaTableViewCell.h
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Wasfa;

@interface WasfaTableViewCell : UITableViewCell

-(void) setWasfa:(Wasfa *) wasfa;

@end
