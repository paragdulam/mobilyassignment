//
//  MAAppManager.h
//  MobilyAssignment
//
//  Created by Parag Dulam on 27/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAAppManager : NSObject

+(void) saveWasfas:(NSArray *)wasfas;
+(void) saveWasfa:(NSDictionary *) wasfaDict;

@end
