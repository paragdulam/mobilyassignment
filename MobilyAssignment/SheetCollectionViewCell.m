//
//  SheetCollectionViewCell.m
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "SheetCollectionViewCell.h"
#import "MAImageView.h"
#import "Sheet.h"

@interface SheetCollectionViewCell ()
{
    MAImageView *imageView;
}

@end

@implementation SheetCollectionViewCell


-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        imageView = [[MAImageView alloc] initWithFrame:CGRectZero];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:imageView];
    }
    return self;
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    imageView.frame = self.contentView.bounds;
}

-(void)setSheet:(Sheet *)sheet
{
    [imageView setImageURL:[NSURL URLWithString:sheet.object] forFileId:sheet.sheet_id];
}

@end
