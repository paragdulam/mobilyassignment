//
//  Sheet.m
//  MobilyAssignment
//
//  Created by Parag Dulam on 28/02/15.
//  Copyright (c) 2015 Parag Dulam. All rights reserved.
//

#import "Sheet.h"


@implementation Sheet

@dynamic object;
@dynamic sheet_description;
@dynamic sheet_id;
@dynamic time_word;
@dynamic type;
@dynamic updated_time;
@dynamic view_order;

@end
